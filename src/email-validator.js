const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export function validate(email) {
   // Use a regular expression to match the email address pattern
   const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
   if (!emailRegex.test(email)) {
     return false;
   }
 
   // Check if the domain of the email address is in the list of valid domains
   const domain = email.split('@')[1];
   return VALID_EMAIL_ENDINGS.includes(domain);
 }