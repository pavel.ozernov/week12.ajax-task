import onload from './join-us-section.js';
import "./styles/style.css";
import "./styles/normalize.css";
import {communitySection} from './community';

const requestURLcommunity = 'http://localhost:3000/community';
const requestURLsubscribe = 'http://localhost:3000/subscribe';



//standard or advanced

window.onload = () => {
    onload.create('standard');
    communitySection(requestURLcommunity)
};

