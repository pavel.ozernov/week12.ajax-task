// eslint-disable-next-line import/extensions
import { validate } from './email-validator';
import {postRequest} from "./postRequest"

const onload = {
  create(type) {
    switch (type) {
      case 'standard':
        return this.registerSection('Join Our Program', 'Subscribe');
      case 'advanced':
        return this.registerSection('Join Our Advanced Program', 'Subscribe to Advanced Program');
      default:
        throw new Error(`Section type ${type} is not supported.`);
    }
  },

  registerSection(message, button_text) {
    const existingSection = document.getElementById('app-sect');

    // create a new section element
    const newSection = document.createElement('section');
    newSection.classList.add('app-section');
    newSection.classList.add('app-join-programme');

    // create a new div element and set its display to flex with column direction
    const flexContainer = document.createElement('div');
    flexContainer.classList.add('join_us_flexContainer')
    newSection.appendChild(flexContainer);

    // create three new child elements to add to the flex container
    const childElement1 = document.createElement('h2');
    const childElement2 = document.createElement('div');
    const childElement3 = document.createElement('form');

    childElement3.classList.add('join_us_input_form');

    // add content to each of the child elements
    childElement1.textContent = message;
    childElement2.innerHTML = 'Sed do eiusmod tempor incididunt' + '<br />' + 'ut labore et dolore magna aliqua.';

    // create input field and button elements
    const inputField = document.createElement('input');
    inputField.type = 'text';
    inputField.classList.add('join_us_input');

    // placing localStorage (if any) for onload event in main.js ONLOAD
    if (localStorage.getItem('email')) {
      inputField.value = localStorage.getItem('email');
    } else {
      inputField.placeholder = 'email';
    }

    // local storage EVENT update on inputField   INPUT

    inputField.addEventListener('input', (event) => {
      event.preventDefault();
      //if (validate(inputField.value)) {
        localStorage.setItem('email', inputField.value);
      //}
    });

    // button text and style reference

    const button = document.createElement('button');
    button.classList.add('app-section__button--submit', 'app-section__button');

    if (localStorage.getItem('subscribed') === 'true') {
      button.textContent = 'unsubsribe';
      inputField.style.display = 'none'; // no input field if already subscribed
    } else {
      button.textContent = button_text;
    }

    //= ================================================================
    // prevent default and console log  BUTTON CLICK
    //= ================================================================
    button.addEventListener('click', async (event) => {
      event.preventDefault();
      if (localStorage.getItem('subscribed') === 'true') {
        const requestURL1 = 'http://localhost:3000/unsubscribe';
        const body = {email: localStorage.getItem('email')};
        button.style.opacity = '0.3';
        await postRequest('POST', requestURL1, body)
        .then(data => 
         {
          console.log(data);
          button.style.opacity = '1';
          localStorage.removeItem('email');
          inputField.style.display = 'flex';
          localStorage.setItem('subscribed', 'false');
          button.textContent = button_text;
          inputField.value = '';
          inputField.placeholder = 'email';
         })
         .catch(err => {
          console.log('err');
          alert(err)
         }
          ) 

      } else if (validate(inputField.value)) {
        
        const requestURL = 'http://localhost:3000/subscribe';
        const body = {email: inputField.value};
        button.style.opacity = '0.3';
        await postRequest('POST', requestURL, body)
        .then(data => 
         {
          console.log(data);
          button.style.opacity = '1';
          inputField.style.display = 'none';
          button.textContent = 'Unsubscribe';
          localStorage.setItem('subscribed', 'true');
         })
         .catch(err => {
          console.log(err);
          alert(err.error); // display error message using alert
          button.style.opacity = '1';
         }) 
         }else {
        alert('The email address is incorrect');
         };
          
         
      }

     
    );

    // add the input field and button to the childElement3 div
    childElement3.appendChild(inputField);
    childElement3.appendChild(button);

    childElement1.classList.add('app-title');
    childElement2.classList.add('app-subtitle');

    // set the child elements' heights to 33.33%
    childElement1.style.flex = '1 1 0';
    childElement2.style.flex = '1 1 0';
    childElement3.style.flex = '1 1 0';

    childElement2.style.marginBottom = '20px';

    // add display: flex and justify-content: center to center child elements horizontally
    childElement1.style.display = 'flex';
    childElement1.style.justifyContent = 'center';
    childElement2.style.display = 'flex';
    childElement2.style.justifyContent = 'center';
    childElement3.style.display = 'flex';
    childElement3.style.justifyContent = 'center';

    flexContainer.appendChild(childElement1);
    flexContainer.appendChild(childElement2);
    flexContainer.appendChild(childElement3);

    // insert the new section element after the existing section element
    existingSection.insertAdjacentElement('afterend', newSection);
  },
};




 export default onload;
 