import personIcon_1 from "./assets/images/community_placeholder_01.png";
import personIcon_2 from "./assets/images/community_placeholder_02.png";
import personIcon_3 from "./assets/images/community_placeholder_03.png";
import {postRequest} from "./postRequest"



export function communitySection(requestURLcomty) {
 const existingSection = document.querySelector('#app-sect-read-more');

  // create a new section element
  const newSection = document.createElement('div');
  newSection.classList.add('app-title');
  const newSectionContainer = document.createElement('div');
  newSection.classList.add('community_sectionContainer');

  newSection.appendChild(newSectionContainer);

  const newTitleSection = document.createElement('h2');
  const newTitleTextSection = document.createElement('h3');
  const newFlexContainer = document.createElement('div');
  
  newSectionContainer.appendChild(newTitleSection);
  newSectionContainer.appendChild(newTitleTextSection);
  newSectionContainer.appendChild(newFlexContainer);

  newTitleSection.innerHTML = "Big Community of" + "<br />" + "People Like You.";
  
  newTitleTextSection.innerHTML = "We’re proud of our products, and we’re really excited when" + "<br />" + "we get feedback from our users.";
  
 
 
  newTitleSection.classList.add('app-title');
  newTitleTextSection.classList.add('app-subtitle');
  newFlexContainer.classList.add('community_flexContainer');



  // =================function to create card content

  async function addItemToContainer(container, personIcon, i) {
    const { default: imgSrc } = await import(`./assets/images/${personIcon}`);
    const newItem_image = document.createElement('img');
    newItem_image.src = imgSrc;
    newItem_image.classList.add('newItemIcon');
    const newItem_text = document.createElement('div');
    const newItem_name = document.createElement('div');
    const newItem_position = document.createElement('div');
  
    container.appendChild(newItem_image);
    container.appendChild(newItem_text);
    container.appendChild(newItem_name);
    container.appendChild(newItem_position);
  
    newItem_text.classList.add('newItem_text');
    newItem_name.classList.add('newItem_name');
    newItem_position.classList.add('newItem_position');
    
   //sample community_member value 

    postRequest('GET', requestURLcomty)
    .then(data => 
     {newItem_image.src = data[i-1]['avatar'];
     newItem_text.textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
     newItem_name.textContent = data[i-1]['firstName'] + ' ' + data[i-1]['lastName'];
     newItem_position.textContent = data[i-1]['position'];
     })
    .catch(err => {
    newItem_text.textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
    newItem_name.textContent = 'NamePlaceHolder';
    newItem_position.textContent = 'PositionPlaceHolder';
    })
  };

  // =========================loop in the creation of card content for a number of cards

  const numCards = 3;

  for (let i = 1; i <= numCards; i++) {
    const container = document.createElement('div');
    container.id = `newCard_${i}`;
    container.classList.add('community_card');
    newFlexContainer.appendChild(container);
    const personIcon = `community_placeholder_0${i}.png`;

    addItemToContainer(container, personIcon, i);

    // hover function
    container.addEventListener("mouseover", () => {
      container.style.boxShadow = "0px 0px 15px #8f8e8e";
    });

    container.addEventListener("mouseout", () => {
      container.style.boxShadow = "none";
    });

  };




//   // insert the new section element after the existing section element
 existingSection.insertAdjacentElement('afterend', newSection);

};
